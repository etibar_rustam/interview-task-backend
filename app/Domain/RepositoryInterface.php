<?php

namespace App\Domain;

use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    /**
     *  Add the appropriate model to the repository.
     * @return Model
     */
    public function getModel(): Model;

    /**
     * Get item by id.
     * @param string $id
     * @return Model
     */
    public function getById(string $id): Model;

    /**
     * Save the item.
     * @param array $data
     * @return Model
     */
    public function save(array $data): Model;

    /**
     * Save many items.
     * @param array $data
     * @return bool
     */
    public function saveMany(array $data): bool;

    /**
     * Update the item.
     * @param string $id
     * @param array $data
     * @return bool
     */
    public function update(string $id, array $data): bool;

    /**
     * Delete the item.
     * @param string $id
     * @return bool
     */
    public function delete(string $id): bool;
}
