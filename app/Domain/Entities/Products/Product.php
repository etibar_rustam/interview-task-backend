<?php

declare(strict_types=1);

namespace App\Domain\Entities\Products;

use App\Domain\Factories\ProductFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @return ProductFactory
     */
    protected static function newFactory(): ProductFactory
    {
        return new ProductFactory();
    }
}
