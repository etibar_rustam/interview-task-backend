<?php

declare(strict_types=1);

namespace App\Domain\Entities\Companies;

use App\Domain\Factories\CompanyFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @return CompanyFactory
     */
    protected static function newFactory(): CompanyFactory
    {
        return new CompanyFactory();
    }
}
