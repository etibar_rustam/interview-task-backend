<?php

declare(strict_types=1);

namespace App\Domain\Entities\Invoices;

use App\Domain\Entities\HasApproval;
use App\Domain\Entities\Products\Product;
use App\Domain\Enums\Currency;
use App\Domain\Enums\StatusEnum;
use App\Domain\Factories\InvoiceFactory;
use App\Domain\ValueObjects\DateRange;
use App\Domain\ValueObjects\InvoiceNumber;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class Invoice extends Model implements HasApproval
{
    use HasFactory;

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var string[]
     */
    protected $fillable = [
        'number',
        'date',
        'due_date',
        'status',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'date' => 'date',
        'due_date' => 'date',
        'status' => StatusEnum::class,
    ];

    public function getNumber(): InvoiceNumber
    {
        return $this->number;
    }

    public function number(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => new InvoiceNumber($value),
        );
    }

    public function getId(): UuidInterface
    {
        return Uuid::fromString($this->id);
    }

    public function getDateRange(): DateRange
    {
        return new DateRange($this->date, $this->due_date);
    }

    public function getCompanyId(): int
    {
        return $this->company_id;
    }

    public function getCurrency(): Currency
    {
        return Currency::USD;
    }

    public function getStatus(): StatusEnum
    {
        return $this->status;
    }

    public function updateStatus(StatusEnum $status): bool
    {
        return $this->update(['status' => $status->value]);
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, InvoiceProductLine::class);
    }

    public function productLines(): HasMany
    {
        return $this->hasMany(InvoiceProductLine::class);
    }

    public function getTotalPrice()
    {
        $this->loadMissing('productLines.product');
        return $this->productLines->sum(fn (InvoiceProductLine $productLine) => $productLine->calculateTotalAmount());
    }

    protected static function newFactory(): InvoiceFactory
    {
        return new InvoiceFactory();
    }
}
