<?php

declare(strict_types=1);

namespace App\Domain\Entities\Invoices;

use App\Domain\Entities\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class InvoiceProductLine extends Model
{
    /**
     * @var string
     */
    protected $keyType = 'string';

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function invoice(): BelongsTo
    {
        return $this->belongsTo(Invoice::class);
    }

    public function calculateTotalAmount(): float
    {
        // Use additional service to calculate quantity, taxes, discounts etc.
        return $this->product->price * $this->quantity;
    }
}
