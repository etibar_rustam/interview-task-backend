<?php

namespace App\Domain\Entities;

use App\Domain\Enums\StatusEnum;

interface HasApproval
{
    /**
     * Update the Entity status.
     * @param StatusEnum $status
     * @return bool
     */
    public function updateStatus(StatusEnum $status): bool;

    /**
     * Get Entity status.
     * @return StatusEnum
     */
    public function getStatus(): StatusEnum;
}
