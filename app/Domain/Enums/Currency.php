<?php

namespace App\Domain\Enums;

enum Currency: string
{
    case USD = 'USD';
}
