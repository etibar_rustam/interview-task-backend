<?php

declare(strict_types=1);

namespace App\Domain\ValueObjects;

use Carbon\Carbon;
use InvalidArgumentException;
use JsonSerializable;

final class DateRange implements JsonSerializable
{
    private Carbon $startDate;

    private Carbon $endDate;

    public function __construct(Carbon $startDate, Carbon $endDate)
    {
        if ($endDate->lt($startDate)) {
            throw new InvalidArgumentException('End date cannot be earlier than start date.');
        }

        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function getStartDate(): Carbon
    {
        return $this->startDate;
    }

    public function getEndDate(): Carbon
    {
        return $this->endDate;
    }

    public function jsonSerialize(): array
    {
        return ['start_date' => $this->getStartDate(), 'end_date' => $this->getEndDate()];
    }
}
