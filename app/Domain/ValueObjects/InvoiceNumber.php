<?php

namespace App\Domain\ValueObjects;

use JsonSerializable;
use Ramsey\Uuid\Uuid;

final class InvoiceNumber implements JsonSerializable
{
    private string $value;

    public function __construct(string $value = null)
    {
        $this->value = $value ?? Uuid::uuid4()->toString();
    }

    public function jsonSerialize(): mixed
    {
        return $this->value;
    }
}
