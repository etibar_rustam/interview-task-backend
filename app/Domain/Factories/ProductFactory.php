<?php

namespace App\Domain\Factories;

use App\Domain\Enums\Currency;
use Illuminate\Database\Eloquent\Factories\Factory;
use Ramsey\Uuid\Uuid;

class ProductFactory extends Factory
{
    private array $randomNames = [
        'pen',
        'pencil',
        'razer',
        'gum',
        'towel',
        'backpack',
        'book',
        'shoes',
        'trousers',
        't-shirt',
        'snickers',
        'water',
        'coca-cola',
        'pepsi',
    ];

    public function definition()
    {
        return [
            'id' => Uuid::uuid4()->toString(),
            'name' => $this->randomNames[array_rand($this->randomNames)],
            'price' => rand(1111, 9999999),
            'currency' => Currency::USD->value,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
