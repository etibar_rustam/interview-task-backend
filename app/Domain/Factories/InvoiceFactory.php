<?php

namespace App\Domain\Factories;

use App\Domain\Entities\Companies\Company;
use App\Domain\Entities\Invoices\Invoice;
use App\Domain\Enums\StatusEnum;
use Illuminate\Database\Eloquent\Factories\Factory;
use Ramsey\Uuid\Uuid;

class InvoiceFactory extends Factory
{
    protected $model = Invoice::class;

    public function definition(): array
    {
        return [
            'id' => Uuid::uuid4()->toString(),
            'number' => $this->faker->uuid(),
            'date' => $this->faker->date(),
            'due_date' => now()->addDays(random_int(1, 30)),
            'company_id' => Company::factory()->create(),
            'status' => StatusEnum::cases()[array_rand(StatusEnum::cases())],
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
