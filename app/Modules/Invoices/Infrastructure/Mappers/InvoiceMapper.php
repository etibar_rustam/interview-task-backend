<?php

namespace App\Modules\Invoices\Infrastructure\Mappers;

use App\Domain\Entities\Invoices\Invoice;
use App\Domain\Entities\Invoices\InvoiceProductLine;
use App\Modules\Invoices\Api\Dto\InvoiceDto;

class InvoiceMapper
{
    public static function toDTO(Invoice $invoice): InvoiceDTO
    {
        $invoice->loadMissing('productLines.product');

        return new InvoiceDTO(
            $invoice->getId(),
            $invoice->getNumber(),
            $invoice->getDateRange(),
            $invoice->getCurrency(),
            $invoice->getStatus(),
            $invoice->getTotalPrice(),
            $invoice->productLines->map(fn (InvoiceProductLine $invoiceProductLine) => [
                'name' => $invoiceProductLine->product->name,
                'quantity' => $invoiceProductLine->quantity,
                'unit_price' => $invoiceProductLine->product->price,
                'amount' => $invoiceProductLine->calculateTotalAmount(),
            ])->toArray()
        );
    }

    public static function toModel(InvoiceDTO $dto, ?Invoice $invoice = null): Invoice
    {
        if ($invoice === null) {
            $invoice = new Invoice();
        }

        $invoice->number = $dto->number;
        $invoice->data_range = $dto->dateRange;
        $invoice->curency = $dto->currency->value;
        $invoice->status = $dto->status->value;

        return $invoice;
    }
}
