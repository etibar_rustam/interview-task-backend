<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application;

use App\Modules\Invoices\Infrastructure\Mappers\InvoiceMapper;
use App\Modules\Invoices\Repositories\InvoiceRepository;
use App\Presentation\ApiResponse;
use Illuminate\Routing\Controller;

class InvoiceController extends Controller
{
    public function __construct(
        private readonly InvoiceService $invoiceService,
        private readonly InvoiceRepository $invoiceRepository,
    ) {
    }

    /**
     * Get Invoice detail.
     * @param string $invoiceId
     * @return ApiResponse
     */
    public function show(string $invoiceId): ApiResponse
    {
        $invoice = $this->invoiceRepository->getById($invoiceId);

        return ApiResponse::success(InvoiceMapper::toDTO($invoice));
    }

    /**
     * Approve Invoice.
     * @param string $invoiceId
     * @return ApiResponse
     */
    public function approve(string $invoiceId): ApiResponse
    {
        $invoice = $this->invoiceRepository->getById($invoiceId);

        $this->invoiceService->approve($this->invoiceRepository->getById($invoiceId));

        return ApiResponse::success(InvoiceMapper::toDTO($invoice));
    }

    /**
     * Reject Invoice.
     * @param string $invoiceId
     * @return ApiResponse
     */
    public function reject(string $invoiceId): ApiResponse
    {
        $invoice = $this->invoiceRepository->getById($invoiceId);

        $this->invoiceService->reject($invoice);

        return ApiResponse::success(InvoiceMapper::toDTO($invoice));
    }
}
