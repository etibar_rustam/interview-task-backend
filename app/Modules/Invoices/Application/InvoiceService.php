<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application;

use App\Domain\Entities\Invoices\Invoice;
use App\Domain\Enums\StatusEnum;
use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;

class InvoiceService
{
    public function __construct(
        private ApprovalFacadeInterface $approvalFacade,
    ) {
    }

    /**
     * Approves the given invoice.
     * @param Invoice $invoice
     * @return bool
     */
    public function approve(Invoice $invoice): bool
    {
        return $this->approvalFacade->approve(new ApprovalDto(
            $invoice,
            StatusEnum::APPROVED,
        ));
    }

    /**
     * Rejects the given invoice.
     * @param Invoice $invoice
     * @return bool
     */
    public function reject(Invoice $invoice): bool
    {
        return $this->approvalFacade->reject(new ApprovalDto(
            $invoice,
            StatusEnum::REJECTED,
        ));
    }
}
