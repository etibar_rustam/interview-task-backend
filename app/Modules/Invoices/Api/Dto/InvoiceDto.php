<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Dto;

use App\Domain\Enums\Currency;
use App\Domain\Enums\StatusEnum;
use App\Domain\ValueObjects\DateRange;
use App\Domain\ValueObjects\InvoiceNumber;
use App\Presentation\ResourceDTOInterface;
use Ramsey\Uuid\UuidInterface;

final class InvoiceDto implements ResourceDTOInterface
{
    public function __construct(
        public UuidInterface $id,
        public InvoiceNumber $number,
        public DateRange $dateRange,
        public Currency $currency,
        public StatusEnum $status,
        public float $totalPrice,
        public array $productLines = [],
    ) {
    }
}
