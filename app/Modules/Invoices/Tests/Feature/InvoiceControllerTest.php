<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Tests\Feature;

use App\Domain\Entities\Invoices\Invoice;
use App\Domain\Enums\StatusEnum;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class InvoiceControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testApproveInvoice(): void
    {
        $invoice = Invoice::factory()->create([
            'status' => StatusEnum::DRAFT,
        ]);

        $this->get(route('invoices.approve', ['invoice_id' => $invoice->id]))
            ->assertOk();

        $this->assertDatabaseHas('invoices', [
            'id' => $invoice->id,
            'status' => StatusEnum::APPROVED,
        ]);
    }

    public function testRejectInvoice(): void
    {
        $invoice = Invoice::factory()->create([
            'status' => StatusEnum::DRAFT,
        ]);

        $this->get(route('invoices.reject', ['invoice_id' => $invoice->id]))
            ->assertOk();

        $this->assertDatabaseHas('invoices', [
            'id' => $invoice->id,
            'status' => StatusEnum::REJECTED,
        ]);
    }

    public function testRejectApprovedInvoice(): void
    {
        $invoice = Invoice::factory()->create([
            'status' => StatusEnum::APPROVED,
        ]);

        $this->get(route('invoices.reject', ['invoice_id' => $invoice->id]))
            ->assertStatus(Response::HTTP_BAD_REQUEST);

        $this->assertDatabaseHas('invoices', [
            'id' => $invoice->id,
            'status' => StatusEnum::APPROVED,
        ]);
    }

    public function testApproveRejectedInvoice(): void
    {
        $invoice = Invoice::factory()->create([
            'status' => StatusEnum::REJECTED,
        ]);

        $this->get(route('invoices.approve', ['invoice_id' => $invoice->id]))
            ->assertStatus(Response::HTTP_BAD_REQUEST);

        $this->assertDatabaseHas('invoices', [
            'id' => $invoice->id,
            'status' => StatusEnum::REJECTED,
        ]);
    }
}
