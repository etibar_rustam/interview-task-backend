<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Tests\Unit;

use App\Domain\Entities\Invoices\Invoice;
use App\Domain\Enums\StatusEnum;
use App\Infrastructure\Exceptions\EntityApprovedException;
use App\Infrastructure\Exceptions\EntityRejectedException;
use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Approval\Application\ApprovalFacade;
use App\Modules\Invoices\Application\InvoiceService;
use Illuminate\Contracts\Events\Dispatcher;
use Mockery;
use PHPUnit\Framework\TestCase;

class InvoiceServiceTest extends TestCase
{
    private ApprovalFacade $approvalFacade;

    protected function setUp(): void
    {
        parent::setUp();

        $dispatcher = Mockery::mock(Dispatcher::class);

        app()->instance(Dispatcher::class, $dispatcher);

        $this->approvalFacade = app()->make(ApprovalFacade::class);
    }

    public function testApproveInvoice(): void
    {
        $invoice = new Invoice([
            'status' => StatusEnum::DRAFT,
        ]);

        $approvalFacade = Mockery::mock(ApprovalFacadeInterface::class);
        $approvalFacade->shouldReceive('approve')->once()
            ->with(Mockery::on(function (ApprovalDto $dto) use ($invoice) {
                return $dto->entity === $invoice && StatusEnum::APPROVED === $dto->status;
            }))
            ->andReturn(true);

        $service = new InvoiceService($approvalFacade);
        $result = $service->approve($invoice);

        self::assertTrue($result);
    }

    public function testRejectInvoice(): void
    {
        $invoice = new Invoice([
            'status' => StatusEnum::DRAFT,
        ]);

        $approvalFacade = Mockery::mock(ApprovalFacadeInterface::class);
        $approvalFacade->shouldReceive('reject')->once()
            ->with(Mockery::on(function (ApprovalDto $dto) use ($invoice) {
                return $dto->entity === $invoice && StatusEnum::REJECTED === $dto->status;
            }))
            ->andReturn(true);

        $service = new InvoiceService($approvalFacade);
        $result = $service->reject($invoice);

        self::assertTrue($result);
    }

    public function testApproveShouldReturnAnErrorIfInvoiceApproved(): void
    {
        $invoice = new Invoice([
            'status' => StatusEnum::APPROVED,
        ]);

        $this->expectException(EntityApprovedException::class);

        $service = new InvoiceService($this->approvalFacade);
        $service->approve($invoice);
    }

    public function testApproveShouldReturnAnErrorIfInvoiceRejected(): void
    {
        $invoice = new Invoice([
            'status' => StatusEnum::REJECTED,
        ]);

        $this->expectException(EntityRejectedException::class);

        $service = new InvoiceService($this->approvalFacade);
        $service->approve($invoice);
    }
}
