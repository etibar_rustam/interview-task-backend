<?php

namespace App\Modules\Invoices\Repositories;

use App\Domain\Entities\Invoices\Invoice;
use App\Domain\RepositoryInterface;

class InvoiceRepository implements RepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function getModel(): Invoice
    {
        return app(Invoice::class);
    }

    /**
     * @inheritDoc
     */
    public function getById(string $id): Invoice
    {
        return $this->getModel()->findOrFail($id);
    }

    /**
     * @inheritDoc
     */
    public function save(array $data): Invoice
    {
        return $this->getModel()->create($data);
    }

    /**
     * @inheritDoc
     */
    public function saveMany(array $data): bool
    {
        return $this->getModel()->insert($data);
    }

    /**
     * @inheritDoc
     */
    public function update(string $id, array $data): bool
    {
        return $this->getById($id)->update($data);
    }

    /**
     * @inheritDoc
     */
    public function delete(string $id): bool
    {
        return  $this->getModel()->whereId($id)->delete();
    }
}
