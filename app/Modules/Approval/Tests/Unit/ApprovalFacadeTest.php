<?php

declare(strict_types=1);

namespace App\Modules\Approval\Tests\Unit;

use App\Domain\Entities\HasApproval;
use App\Domain\Enums\StatusEnum;
use App\Infrastructure\Exceptions\EntityApprovedException;
use App\Infrastructure\Exceptions\EntityRejectedException;
use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Approval\Api\Events\EntityApproved;
use App\Modules\Approval\Api\Events\EntityRejected;
use App\Modules\Approval\Application\ApprovalFacade;
use Illuminate\Contracts\Events\Dispatcher;
use PHPUnit\Framework\TestCase;

class ApprovalFacadeTest extends TestCase
{
    private ApprovalFacadeInterface $approvalFacade;
    private Dispatcher $dispatcher;

    protected function setUp(): void
    {
        $this->dispatcher = $this->createMock(Dispatcher::class);
        $this->approvalFacade = new ApprovalFacade($this->dispatcher);
    }

    public function testApproveShouldDispatchEventIfStatusIsDraft(): void
    {
        $dto = $this->getDTO(StatusEnum::DRAFT);
        $this->dispatcher
            ->expects($this->once())
            ->method('dispatch')
            ->with($this->callback(function (EntityApproved $event) use ($dto) {
                return $event->getDto() === $dto;
            }));

        $this->approvalFacade->approve($dto);

        $this->assertTrue(true);
    }

    public function testsApproveShouldThrowExceptionIfEntityIsAlreadyApproved(): void
    {
        $dto = $this->getDTO(StatusEnum::APPROVED);
        $this->dispatcher
            ->expects($this->never())
            ->method('dispatch');

        $this->expectException(EntityApprovedException::class);

        $this->approvalFacade->approve($dto);
    }

    public function testApproveShouldThrowExceptionIfEntityIsAlreadyRejected(): void
    {
        $dto = $this->getDTO(StatusEnum::REJECTED);
        $this->dispatcher
            ->expects($this->never())
            ->method('dispatch');

        $this->expectException(EntityRejectedException::class);

        $this->approvalFacade->approve($dto);
    }

    public function testRejectShouldDispatchEventIfEntityIsPending(): void
    {
        $dto = $this->getDTO(StatusEnum::DRAFT);
        $this->dispatcher
            ->expects($this->once())
            ->method('dispatch')
            ->with($this->callback(function (EntityRejected $event) use ($dto) {
                return $event->getDto() === $dto;
            }));

        $this->approvalFacade->reject($dto);
    }

    public function testRejectShouldThrowExceptionIfEntityIsAlreadyRejected(): void
    {
        $dto = $this->getDTO(StatusEnum::REJECTED);
        $this->dispatcher
            ->expects($this->never())
            ->method('dispatch');

        $this->expectException(EntityRejectedException::class);

        $this->approvalFacade->reject($dto);
    }

    private function getDTO(StatusEnum $status): ApprovalDto
    {
        $approval = $this->createMock(HasApproval::class);
        $approval->method('getStatus')->willReturn($status);

        return new ApprovalDto($approval, $status);
    }
}
