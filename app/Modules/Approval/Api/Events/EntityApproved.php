<?php

declare(strict_types=1);

namespace App\Modules\Approval\Api\Events;

use App\Modules\Approval\Api\Dto\ApprovalDto;

final readonly class EntityApproved implements ApprovalEventInterface
{
    public function __construct(
        private ApprovalDto $approvalDto
    ) {
    }

    /**
     * @inheritDoc
     */
    public function getDto(): ApprovalDto
    {
        return $this->approvalDto;
    }
}
