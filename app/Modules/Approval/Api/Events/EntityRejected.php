<?php

declare(strict_types=1);

namespace App\Modules\Approval\Api\Events;

use App\Modules\Approval\Api\Dto\ApprovalDto;

final readonly class EntityRejected implements ApprovalEventInterface
{
    public function __construct(
        public ApprovalDto $approvalDto
    ) {
    }

    /**
     * @inheritDoc
     */
    public function getDto(): ApprovalDto
    {
        return $this->approvalDto;
    }
}
