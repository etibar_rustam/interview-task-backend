<?php

declare(strict_types=1);

namespace App\Modules\Approval\Api\Events;

use App\Modules\Approval\Api\Dto\ApprovalDto;

interface ApprovalEventInterface
{
    /**
     * Get approvalDTO.
     * @return ApprovalDto
     */
    public function getDto(): ApprovalDto;
}
