<?php

declare(strict_types=1);

namespace App\Modules\Approval\Api\Dto;

use App\Domain\Entities\HasApproval;
use App\Domain\Enums\StatusEnum;
use App\Presentation\ResourceDTOInterface;

final readonly class ApprovalDto implements ResourceDTOInterface
{
    public function __construct(
        public HasApproval $entity,
        public StatusEnum $status,
    ) {
    }
}
