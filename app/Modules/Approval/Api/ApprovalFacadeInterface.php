<?php

declare(strict_types=1);

namespace App\Modules\Approval\Api;

use App\Modules\Approval\Api\Dto\ApprovalDto;

interface ApprovalFacadeInterface
{
    /**
     * Approve an invoice.
     * @param ApprovalDto $entity
     * @return true
     */
    public function approve(ApprovalDto $entity): true;

    /**
     * Reject an invoice.
     * @param ApprovalDto $entity
     * @return true
     */
    public function reject(ApprovalDto $entity): true;
}
