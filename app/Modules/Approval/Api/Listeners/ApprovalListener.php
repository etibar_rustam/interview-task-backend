<?php

declare(strict_types=1);

namespace App\Modules\Approval\Api\Listeners;

use App\Modules\Approval\Api\Events\ApprovalEventInterface;
use Illuminate\Support\Facades\Log;
use Throwable;

final readonly class ApprovalListener
{
    public function handle(ApprovalEventInterface $event): void
    {
        try {
            $event->getDto()->entity->updateStatus($event->getDto()->status);
        } catch (Throwable $exception) {
            Log::error('approve_entity_listener', (array) $exception->getMessage());
        }
    }
}
