<?php

declare(strict_types=1);

namespace App\Modules\Approval\Application;

use App\Domain\Enums\StatusEnum;
use App\Infrastructure\Exceptions\EntityApprovedException;
use App\Infrastructure\Exceptions\EntityRejectedException;
use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Approval\Api\Events\EntityApproved;
use App\Modules\Approval\Api\Events\EntityRejected;
use Exception;
use Illuminate\Contracts\Events\Dispatcher;

final readonly class ApprovalFacade implements ApprovalFacadeInterface
{
    public function __construct(
        private Dispatcher $dispatcher
    ) {
    }

    /**
     * @inheritDoc
     */
    public function approve(ApprovalDto $dto): true
    {
        $this->validate($dto);
        $this->dispatcher->dispatch(new EntityApproved($dto));

        return true;
    }

    /**
     * @inheritDoc
     */
    public function reject(ApprovalDto $dto): true
    {
        $this->validate($dto);
        $this->dispatcher->dispatch(new EntityRejected($dto));

        return true;
    }

    /**
     * Validate the entity status.
     * @throws EntityRejectedException
     * @throws EntityApprovedException
     * @throws Exception
     */
    private function validate(ApprovalDto $dto): void
    {
        switch ($dto->entity->getStatus()) {
            case StatusEnum::APPROVED:
                throw new EntityApprovedException();
            case StatusEnum::REJECTED:
                throw new EntityRejectedException();
        }
    }
}
