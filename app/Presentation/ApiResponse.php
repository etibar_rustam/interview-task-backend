<?php

declare(strict_types=1);

namespace App\Presentation;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

final class ApiResponse extends JsonResponse
{
    /**
     * API response instance.
     * @param ResourceDTOInterface|null $resourceDTO
     * @param int $statusCode
     */
    public function __construct(ResourceDTOInterface $resourceDTO = null, int $statusCode = Response::HTTP_OK)
    {
        $data = [
            'message' => 'Success',
            'data' => $resourceDTO,
            'errors' => (object) [],
        ];

        parent::__construct($data, $statusCode);
    }

    /**
     * Success response.
     * @param ResourceDTOInterface|null $resourceDTO
     * @return $this
     */
    final public static function success(ResourceDTOInterface $resourceDTO = null): self
    {
        return new self($resourceDTO);
    }

    /**
     * Fail response.
     * @param array $errors
     * @return $this
     */
    final public static function fail(array $errors = []): self
    {
        return (new self(null, Response::HTTP_BAD_REQUEST))
            ->setMessage('Fail')
            ->setErrors($errors);
    }

    /**
     * Set errors.
     * @param array $errors
     * @return $this
     */
    private function setErrors(array $errors): self
    {
        $data = (array) $this->getData();
        $this->setData(Arr::set($data, 'errors', $errors));

        return $this;
    }

    /**
     * Set message.
     * @param string $message
     * @return $this
     */
    public function setMessage(string $message): self
    {
        $data = (array) $this->getData();
        $this->setData(Arr::set($data, 'message', $message));

        return $this;
    }


    /**
     * Set validation errors.
     * @param ValidationException $exception
     * @return $this
     * @throws Throwable
     */
    final public function setValidationExceptionErrors(ValidationException $exception): self
    {
        $this->setErrors($exception->errors())->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);

        return $this;
    }
}
