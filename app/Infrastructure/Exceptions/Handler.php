<?php

declare(strict_types=1);

namespace App\Infrastructure\Exceptions;

use App\Presentation\ApiResponse;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     */
    public function register(): void
    {
        $this->renderable(function (EntityApprovedException $e): ApiResponse {
            return ApiResponse::fail()->setMessage('Entity already approved');
        });
        $this->renderable(function (EntityRejectedException $e): ApiResponse {
            return ApiResponse::fail()->setMessage('Entity already rejected');
        });

        $this->renderable(function (Throwable $e) {
            return ApiResponse::fail()
                ->setStatusCode(Response::HTTP_SERVICE_UNAVAILABLE)
                ->setMessage('Service unavailable');
        });
    }

    /**
     * {@inheritdoc}
     * @throws Throwable
     */
    protected function invalidJson($request, ValidationException $exception): ApiResponse
    {
        return ApiResponse::fail()->setMessage($exception->getMessage())->setValidationExceptionErrors($exception);
    }
}
