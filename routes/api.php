<?php

declare(strict_types=1);

use App\Modules\Invoices\Application\InvoiceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//dd(\App\Domain\Entities\Invoices\Invoice::all());
Route::prefix('invoices')->name('invoices.')->controller(InvoiceController::class)->group(function (): void {
    Route::prefix('{invoice_id}')->group(function (): void {
        Route::get('/', 'show')->name('show');
        Route::get('approve', 'approve')->name('approve');
        Route::get('reject', 'reject')->name('reject');
    });
});
